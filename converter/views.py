from django.http import HttpResponse
from django.shortcuts import render
from .util import NumberToText


def index(request):
    return render(request, 'converter/index.html')


def result(request):
    number = request.POST.get('number_to_convert', 'false')
    number_temp = number
    is_number = number_temp.replace("-", "").isdigit()
    if is_number:
        converter = NumberToText(number)
        result_text = converter.get_text()
        context = {
            'result_text': result_text,
            'is_number': True
        }
    else:
        context = {
            'is_number': False,
        }
    return render(request, 'converter/result.html', context)
