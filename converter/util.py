class NumberToText:
    _units = {1: "jeden", 2: "dwa", 3: "trzy", 4: "cztery", 5: "pięć", 6: "sześć", 7: "siedem", 8: "osiem",
              9: "dziewięć"}
    _tens = {1: "dziesięć", 2: "dwadzieścia", 3: "trzydzieści", 4: "czterdzieści", 5: "pięćdziesiąt", 6: "sześćdziesiąt"
        , 7: "siedemdziesiąt", 8: "osiemdziesiąt", 9: "dziewięćdziesiąt"}
    _hundreds = {1: "sto", 2: "dwieście", 3: "trzysta", 4: "czterysta", 5: "pięćset", 6: "sześćset", 7: "siedemset"
        , 8: "osiemset", 9: "dziewięćset"}
    _ten_to_twenty = {11: "jedenaście", 12: "dwanaście", 13: "trzynaście", 14: "czternaście", 15: "piętnaście"
        , 16: "szesnaście", 17: "siedemnaście", 18: "osiemnaście", 19: "dziewiętnaście"}
    _thousands = {1: {0: "tysiąc", 1: "tysięcy", 2: "tysiące"}, 2: {0: "milion", 1: "milionów", 2: "miliony"}
        , 3: {0: "miliard", 1: "miliardów", 2: "miliardy"}, 4: {0: "bilion", 1: "bilionów", 2: "biliony"}
        , 5: {0: "biliard", 1: "biliardów", 2: "biliardy"}, 6: {0: "trylion", 1: "trylionów", 2: "tryliony"}}

    def __init__(self, number):
        self._number = number
        self._position = 0
        self._raw_number_input = number
        self._minus_present = False

    def get_text(self):
        self._position = 0
        self._prepare_number()
        self._process_minus()

        if len(self._number) == 1 and '0' in self._number:
            return "zero"

        output_array = []
        digits_amount = len(self._number)
        skip_next = False

        while self._position < digits_amount:
            if skip_next:
                skip_next = False
                self._increase_position()
                continue

            last_two_digits = self._get_last_two_digits()
            digit = int(self._number[self._position])

            name_source = self._get_name_source()
            thousands_name = self._get_thousands_name()
            skip_next = self._get_skip_next()

            if len(thousands_name) > 0:
                output_array.append(thousands_name)

            if digit in name_source:
                output_array.append(name_source[digit])
            elif last_two_digits in name_source:
                output_array.append(name_source[last_two_digits])

            self._increase_position()

        if self._minus_present:
            output_array.append("minus")

        output_array.reverse()
        output = ' '.join(output_array)
        return output

    def _get_thousands_variant(self, last_two_digits):
        last_two_digits = int(last_two_digits)
        if last_two_digits == 0:
            variant = 1
        elif (last_two_digits % 10) in {2, 3, 4}:
            if int(last_two_digits / 10) == 1:
                variant = 1
            else:
                variant = 2
        elif last_two_digits == 1:
            if self._get_last_three_digits() == 1:
                variant = 0
            else:
                variant = 1
        else:
            variant = 1

        return variant

    def _prepare_number(self):
        self._number = list(self._number)
        self._number.reverse()

    def _process_minus(self):
        if "-" in self._number:
            self._minus_present = True
            self._number.remove("-")

    def _get_last_two_digits(self):
        if self._position + 1 < len(self._number):
            last_two_digits = int(self._number[self._position]) + int(self._number[self._position + 1]) * 10

        else:
            last_two_digits = int(self._number[self._position])

        return last_two_digits

    def _get_last_three_digits(self):
        if self._position + 2 < len(self._number):
            last_three_digits = self._get_last_two_digits() + 100 * int(self._number[self._position + 2])
        else:
            last_three_digits = self._get_last_two_digits()

        return last_three_digits

    def _increase_position(self):
        self._position += 1

    def _get_name_source(self):
        last_two_digits = self._get_last_two_digits()
        if self._position % 3 == 0:
            if 10 < last_two_digits < 20:
                name_source = self._ten_to_twenty
            else:
                name_source = self._units

        elif self._position % 3 == 1:
            name_source = self._tens

        else:
            name_source = self._hundreds
        return name_source

    def _get_skip_next(self):
        skip_next = False
        if self._position % 3 == 0:
            last_two_digits = self._get_last_two_digits()
            if 10 < last_two_digits < 20:
                skip_next = True
        return skip_next

    def _get_thousands_name(self):
        last_two_digits = self._get_last_two_digits()
        thousands_name = ""
        if self._position % 3 == 0:
            thousands_key = int(self._position / 3)
            if thousands_key in self._thousands:
                variant = self._get_thousands_variant(last_two_digits)
                thousands_name = self._thousands[thousands_key][variant]

        return thousands_name
